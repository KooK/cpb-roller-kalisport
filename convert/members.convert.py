#! /usr/bin/env python

from collections import defaultdict
import csv
from pathlib import Path, PurePath
import sys
import yaml
from yaml.loader import SafeLoader

# ========================================
try:
    db_filepath = sys.argv[1]
except IndexError:
    db_filepath = "2023_members.csv"

filename = PurePath(db_filepath).stem
year = filename.split("_")[0]
print(f"fichier d'entrée : {db_filepath} -> {year}")

prog_dir = Path(__file__).parent
print(f"chemin du script: {prog_dir}")

# -----------------------------------------
# constantes
d_sex = {
    "0": "Féminin",
    "1": "Masculin",
}

# ========================================
def load_conversion():
    with open(f"{prog_dir}/conversion.yml") as fe:
        convers = yaml.load(fe,Loader=SafeLoader)
        # ~ print(convers["adherent"])

    return convers

def load_lesson(year):
    d_lessons = dict()
    lesson_filename = year + "_lessons.reformatted.csv"
    with open(lesson_filename, newline='') as fe:
        reader = csv.DictReader(fe)
        for d_row in reader:
            # ~ print(d_row)
            id_ = d_row['id']
            lesson = d_row['short']
            d_lessons[id_] = lesson
    # ~ print(d_lessons)
    return d_lessons

def load_member_lessons(year):
    d_lessons = load_lesson(year)

    member_lessons_filename = year + "_subscribe.csv"
    d_members = defaultdict(list)
    with open(member_lessons_filename, newline='') as fe:
        reader = csv.DictReader(fe)
        for d_row in reader:
            # ~ print(d_row)
            id_ = d_row['id_member']
            lesson = d_row['id_lesson']
            short = d_lessons[lesson]
            d_members[id_].append(short)
    # ~ print(d_members)
    return d_members

# -----------------------------------------
# id_lesson => short

print( "Traitement des données" )
print( "déterminer id_adhérent => liste d'équipes" )
d_member_equipes = load_member_lessons(year)

d_convers = load_conversion()
l_colonnes = list(d_convers["adherent"].values())
l_colonnes.append("CATEGORIE")
l_colonnes.append("EQUIPES")

output_file = "2023_members.reformated.csv"
with open(db_filepath, newline='') as fe, open(output_file, "w", newline='') as fs:
    reader = csv.DictReader(fe)
    writer = csv.DictWriter(fs, fieldnames=l_colonnes, )
    writer.writeheader()
    
    for d_row in reader:
        # print(d_row)
        d_new = { d_convers["adherent"][k]:v for k,v in d_row.items() }
        d_new["SEXE"] = d_sex[d_new["SEXE"]]
        l_equipes = d_member_equipes[d_new['ID']]
        if not l_equipes:
            continue

        d_new["EQUIPES"] = ','.join(l_equipes)

        # TODO
        d_new["CATEGORIE"] = "Adulte"
        # ~ print(d_new)
        writer.writerow(d_new)
        print(f"{d_new['NOM']} {d_new['PRENOM']} - OK")
