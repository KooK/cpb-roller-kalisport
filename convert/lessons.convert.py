#! /usr/bin/env python

import csv
from pathlib import Path, PurePath
import sys
import yaml
from yaml.loader import SafeLoader

# ----------------------
def reformat(in_filepath):
    print( f"in : {in_filepath}" )
    outfile = in_filepath.stem + '.reformatted.csv'
    print( f"out : {outfile}" )

    print( "Traitement des données" )
    t_colonnes = ("id", "nom", "short", )
    with open(in_filepath, newline='') as fe, open(outfile, "w", newline='') as fs:
        reader = csv.DictReader(fe)
        writer = csv.DictWriter(fs, fieldnames=t_colonnes, )
        writer.writeheader()
        for d_row in reader:
            # ~ print(d_row)
            d_new = { k:v for k,v in d_row.items() if k in t_colonnes}  # no 'short' here
            writer.writerow( d_new )

    print( "reformattage terminé" )


# ----------------------
db_dirpath = sys.argv[1]
o_dir = Path(db_dirpath)
for filepath in o_dir.glob('*_lessons.csv'):
    reformat(filepath)
    print("")  # empty line

print("Il faut maintenant éditer les fichiers pour ajouter les noms courts :)")
