# Usage:

## pré-requis

Avoir exporté la BD historique en fichier csv
    Actuellement on utilise tous les fichiers `2023_lessons.csv`, `2023_subscribe.csv` et `members.csv`.  
    On va renommer `members.csv` en `2023_members.csv`.

## reformattage du fichier d'équipe

Pour les équipes, on a besoin de lier les IDs dans le fichier `lessons` au nouveau nom court.  
On commence par extraire les 2 1e colonnes du fichier. J'utilise le script `lessons.convert.py` pour virer les `"`.

```shell
python lessons.convert.py cpb_roller.22_23.sauvegarde_bdd_csv_20221029

# ça génère les fichiers pour toutes les années
```

Ensuite on édite le fichier 2023 à la main pour ajouter les noms courts dans la dernière colonne.  
ça donne ça
```
id,nom,short
12,Roller Fitness / Fit'n'roller,FITNESS
13,Loisir - niveau 3,NIV3
21,Rando perfectionnement,RANDO
30,Enfants - niveau 1 (roue jaune),ENF1
31,Enfants - niveau 2 (roue verte),ENF2
32,Enfants - niveau 3 (roue bleue),ENF3
33,Enfants - niveau 4 (roue rouge),ENF4
34,Hockey - loisir,HOCKEY
35,Loisir - niveau 1,NIV1
41,Slalom (tous niveaux),SLALOM
43,Loisir - niveau 2,NIV2
61,Enduroller,ENDU
62,Parents/enfants,PARENFANT
80,Rink Hockey Compétition,RINK
```

## reformattage du fichier adhérent

Le fichier adhérent doit contenir tous les champs obligatoires et ceux qui semblent pertinents.  

```shell
python members.convert.py 2023_members.csv

# génère le fichier 2023_members.reformated.csv
```

## importation

Dans le site Kalisport
Perso / adhérent
[...] (à coté du bouton "Ajouter" en haut à gauche) et "Importer"
Ensuite le site valide les données et on peut faire l'import.
